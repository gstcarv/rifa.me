﻿create database rifas_wallace
  default character set utf8
  default collate utf8_general_ci;
  
use rifas_wallace;

create table tb_anunciantes (
  id_anunciante int primary key auto_increment,
  nome varchar(255) not null,
  telefone varchar(30) not null,
  email varchar(255) not null,
  senha varchar(255) not null
) default charset = utf8;
  
alter table tb_anunciantes
add column facebook varchar(400) default null;

alter table tb_anunciantes
add column instagram varchar(400) default null;

alter table tb_anunciantes
add column whatsapp varchar(400) default null;

alter table tb_anunciantes
add column admin boolean not null default 0;

alter table tb_anunciantes
add column tem_permissao boolean not null default 0;

alter table tb_anunciantes
add column alterado boolean default 0;

create table tb_rifas (
  id_rifa int primary key auto_increment,
  id_anunciante int not null,
  titulo varchar(255) not null,
  valor double not null,
  descricao text not null,
  num_bilhetes int not null,
  dt_criacao date not null,
  foreign key (id_anunciante) references tb_anunciantes(id_anunciante)
) default charset = utf8;

alter table tb_rifas
add column nome_ganhador varchar(255) default null,
add column tel_ganhador varchar(255) default null,
add column bilhete_sorteado int default null,
add column total_pago double default null,
add column dt_finalizacao datetime default null;

create table tb_bilhetes (
  id_bilhete int primary key auto_increment,
  id_rifa int not null,
  numero_bilhete int not null,
  nome_comprador varchar(255) default null,
  telefone_comprador varchar(255) default null,
  dt_reserva datetime default null,
  dt_validade datetime default null,
  dt_pagamento datetime default null,
  foreign key (id_rifa) references tb_rifas(id_rifa)
) default charset = utf8;

create table tb_imagens (
  id_imagem int primary key auto_increment,
  id_rifa int,
  nome_imagem varchar(255) not null
) default charset = utf8;

create table tb_bancos (
  id_banco int auto_increment primary key,
  nome_banco varchar(255),
  imagem_banco varchar(255)
) default charset = utf8;

insert into tb_bancos 
(nome_banco, imagem_banco) values 
('Itaú', 'itau.png'),
('Banco do Brasil', 'bb.png'),
('Bradesco', 'bradesco.png'),
('Caixa', 'caixa.png'),
('Santander', 'santander.png'),
('Banco Inter', 'inter.png'),
('Banco Original', 'original.png');

create table tb_banco_anunciante (
  id_banco_anunciante int auto_increment primary key,
  id_anunciante int not null,
  id_banco int not null,
  titular varchar(255) not null,
  cpf varchar(30) not null,
  agencia varchar(20) not null,
  conta varchar(10) not null 
) default charset = utf8;