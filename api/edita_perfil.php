<?php

include('conn.php');

session_start();

// ID DO USUÁRIO PARA EDITAR
$user = $_SESSION['logged_user']['id_anunciante'];

if($_POST){
    // PARAMETROS
    $nome = $_POST['nome'];
    $telefone = $_POST['telefone'];
    $senha = $_POST['senha'];

    // CRIPTOGRAFA A SENHA EM MD5
    $md5pass = md5($senha);

    // SÓ EDITA A SENHA SE ELA NÃO ESTIVER VAZIA
    $editSenha = $senha == '' ? '' : ", senha='$md5pass'";

    // ENVIA ATUALIZAÇÃO PARA O BANCO
    $query = "UPDATE tb_anunciantes SET
                nome='$nome',
                telefone='$telefone'
                $editSenha
            WHERE id_anunciante='$user'";
    $conn->query($query);

    // ATUALIZA AS SESSÕES
    $_SESSION['logged_user']['nome'] = $nome;
    $_SESSION['logged_user']['telefone'] = $telefone;

    // SÓ ATUALIZA A SESSÃO DA SENHA SE ELA FOR ATUALIZADA
    if($editSenha != ''){
        $_SESSION['logged_user']['senha'] = $md5pass;
    }

    // REDIRECIONA PARA O PERFIL
    $_SESSION['atualizado'] = 'perfil';
    header('location: /perfil');
}


?>