<?php 

    include('conn.php');

    // PEGA AS IMAGENS ENVIADAS
    $images = [];

    for($i = 1; $i <= 8; $i++){
        if(isset($_FILES["imagem_$i"])){
            $images[] = $_FILES["imagem_$i"];
        }
    }
    
    include_once('../includes/Compress.php');

    session_start();

    // PARÂMETROS
    $dateTimeNow = date("Y-m-d H:i:s");
    $dateNow = date("Y-m-d");
    $titulo = $_POST['titulo'];
    $replaced_images = $_POST['replaced_images'];
    $id_rifa = $_POST['id_rifa'];
    $descricao = nl2br($_POST['descricao']);

    // ATUALIZA A RIFA NO BANCO
    $updateRifa = "UPDATE tb_rifas SET
                   titulo='$titulo',
                   descricao='$descricao'
                   WHERE id_rifa='$id_rifa'";

    $conn->query($updateRifa);

    try {

        // SE ALGUMA IMAGEM FOR APAGADA OU SUBSTITUIDA
        if($replaced_images){
            // PEGA AS ID'S DAS IMAGENS SUBSTITUIDAS
            $toReplace = json_decode($replaced_images);
            $toReplace = implode(',', $toReplace);

            // DELETA AS IMAGENS SUBSTITUIDAS DO BANCO
            $conn->query("DELETE FROM tb_imagens WHERE id_imagem IN ($toReplace)");
        }

        // SE TIVER IMAGENS
        if(count($images) > 0){
            // QUERY INICIAL
            $inserts = "INSERT INTO tb_imagens (id_rifa, nome_imagem) VALUES ";

            // PARA CADA IMAGEM QUE O ANUNCIANTE ENVIOU
            foreach($images as $key => $image){
                // PEGA O NOME DA IMAGEM
                $tmp_name = $image['tmp_name'];
                // PEGA O TIPO DA IMAGEM
                $type = explode('/', $image['type'])[1];
                // NOME DO ARQUIVO DA IMAGEM [QUE FICARÁ SALVA NO SERVIDOR]
                $filename = md5($dateTimeNow . $tmp_name) . '.' . $type;
                // ENVIA A IMAGEM PARA A PASTA /imagens/uploads
                move_uploaded_file($tmp_name, "../imagens/uploads/$filename");

                // CRIA UMA THUMBNAIL DA IMAGEM [UMA IMAGEM COM QUALIDADE MENOR]
                compress("../imagens/uploads/$filename", "../imagens/uploads/thumb_$filename", 0);

                // VE SE É A ULTIMA IMAGEM
                $comma = (1 + $key) != count($images) ? ", " : ";";
                // CONCATENA A IMAGEM NA QUERY
                $inserts .= "('$id_rifa', '$filename')" . $comma;
            }
                
            // QUANDO A QUERY ESTIVER COMPLETA, ENVIA PRO BANCO
            $conn->query($inserts);
        }

    } catch (Exception $e) {
        echo $e->getMessage();
    }
    

?>