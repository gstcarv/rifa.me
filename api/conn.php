<?php

    // AUTENTICAÇÃO DO BANCO
    $dbhost = "localhost";
    $dbuser = "root";
    $dbpassword = "";
    $dbname = "rifas_wallace";

    // CONEXÃO NO BANCO
    $conn = new mysqli($dbhost, $dbuser, $dbpassword, $dbname);

    // CONFIGURAÇÕES
    $conn->query("SET NAMES 'utf8'");
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');

?>