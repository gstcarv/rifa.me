<?php

session_start();

// SÓ APAGA O ANUNCIANTE SE O USUÁRIO LOGADO FOR ADMIN
if($_SESSION['logged_user']['admin'] == true){
    include('conn.php');
    
    // PARAMETROS
    $id_anunciante = $_POST['id_anunciante'];
    $conn->query("SET FOREIGN_KEY_CHECKS=0;");

    // APAGA AS RIFAS DO ANUNCIANTE E O ANUNCIANTE
    $queries = [
        "DELETE FROM tb_rifas WHERE id_anunciante = $id_anunciante",
        "DELETE FROM tb_anunciantes WHERE id_anunciante = $id_anunciante"
    ];

    foreach($queries as $query){
        $conn->query($query);
    }

    $conn->query("SET FOREIGN_KEY_CHECKS=1;");
    echo $conn->error;
}

?>