<?php 


    include('conn.php');

    // PEGA AS IMAGENS ENVIADAS
    $images = [];

    for($i = 1; $i <= 8; $i++){
        if(isset($_FILES["imagem_$i"])){
            $images[] = $_FILES["imagem_$i"];
        }
    }

    // CRIA O DIRETÓRIO SE NÃO EXISTIR
    if(!is_dir("../imagens/uploads")){
        mkdir("../imagens/uploads", 0700);
    }

    include_once('../includes/Compress.php');
    
    session_start();

    // PARÂMETROS DA RIFA
    $dateTimeNow = date("Y-m-d H:i:s");
    $dateNow = date("Y-m-d");
    $anunciante = $_SESSION['logged_user']['id_anunciante'];
    $titulo = $_POST['titulo'];
    $valor = $_POST['valor'];
    $num_bilhetes = $_POST['num_bilhetes'];
    $descricao = nl2br($_POST['descricao']);

    // ADICIONA A RIFA NO BANCO DE DADOS
    $insertRifa = "INSERT INTO tb_rifas (id_anunciante, titulo, valor, descricao, num_bilhetes, dt_criacao) VALUES
                   ('$anunciante', '$titulo', '$valor', '$descricao', '$num_bilhetes', '$dateNow')";

    $conn->query($insertRifa);

    try {
        if($conn->affected_rows > 0){
            $last_id = $conn->insert_id;

            // INSERE OS BILHETES DA RIFA ADICIONADA
            $inserts = "INSERT INTO tb_bilhetes (id_rifa, numero_bilhete) VALUES ";
            for($i = 0; $i < $num_bilhetes; $i++){
                $comma = $i == ($num_bilhetes - 1) ? "; " : ", ";
                $inserts .= "('$last_id', '$i')" . $comma;
            }

            $conn->query($inserts);

            // SE TIVER IMAGENS
            if(count($images) > 0){
                // QUERY INICIAL
                $inserts = "INSERT INTO tb_imagens (id_rifa, nome_imagem) VALUES ";
                
                // PARA CADA IMAGEM QUE O ANUNCIANTE ENVIOU
                foreach($images as $key => $image){
                    // PEGA O NOME DA IMAGEM
                    $tmp_name = $image['tmp_name'];
                    // PEGA O TIPO DA IMAGEM
                    $type = explode('/', $image['type'])[1];
                    // NOME DO ARQUIVO DA IMAGEM [QUE FICARÁ SALVA NO SERVIDOR]
                    $filename = md5($dateTimeNow . $tmp_name) . '.' . $type;
                    // ENVIA A IMAGEM PARA A PASTA /imagens/uploads
                    move_uploaded_file($tmp_name, "../imagens/uploads/$filename");

                    // CRIA UMA THUMBNAIL DA IMAGEM [UMA IMAGEM COM QUALIDADE MENOR]
                    compress("../imagens/uploads/$filename", "../imagens/uploads/thumb_$filename", 0);

                    // VE SE É A ULTIMA IMAGEM
                    $comma = (1 + $key) != count($images) ? ", " : ";";
                    // CONCATENA A IMAGEM NA QUERY
                    $inserts .= "('$last_id', '$filename')" . $comma;
                }
                
                // QUANDO A QUERY ESTIVER COMPLETA, ENVIA PRO BANCO
                $conn->query($inserts);
            }
            echo $conn->error;

            echo $last_id;
        } else {
            throw new Exception($conn->error);
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
    

?>