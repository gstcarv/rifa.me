<?php

    include('conn.php');

    // ID DA RIFA
    $id_rifa = $_POST['id_rifa'];

    // PEGA O ANUNCIANTE DA RIFA
    $select = $conn->query("SELECT id_anunciante FROM tb_rifas WHERE id_rifa = '$id_rifa'");
    $anunciante = $select->fetch_array(MYSQLI_ASSOC)['id_anunciante'];

    session_start();
    
    $user = $_SESSION['logged_user'];
   
    // APAGA A RIFA SOMENTE SE O USUÁRIO LOGADO FOR O DONO DA RIFA OU ADMINISTRADOR
    if(isset($user)){
        if($user['id_anunciante'] == $anunciante || $user['admin'] == '1'){
            // DELETE AS IMAGENS, BILHETES E AS RIFAS
            $deletes = [
                "DELETE FROM tb_imagens WHERE id_rifa='$id_rifa'",
                "DELETE FROM tb_bilhetes WHERE id_rifa='$id_rifa'",
                "DELETE FROM tb_rifas WHERE id_rifa='$id_rifa'"
            ];
            
            foreach($deletes as $delete){
                $conn->query($delete);
            }
        } else {
            echo "forbidden";
        }
    } else {
        echo "forbidden";
    }

?>