<?php 

    include('conn.php');

    // PEGA A DATA ATUAL
    $now = date("Y-m-d H:i:s");

    // VALIDADE = DATA ATUAL + 2 DIAS
    $validade = new DateTime($now);
    $validade->modify('+2 days');
    $validade = $validade->format('Y-m-d H:i:s');
    
    // PARÂMETROS
    $numeros = implode(',', $_POST['numeros_reserva']);
    $nome_comprador = $_POST['nome_comprador'];
    $tel_comprador = $_POST['tel_comprador'];

    // ID DA RIFA
    $id_rifa = $_POST['id_rifa'];

    // ENVIA AS ATUALIZACOES PARA O BANCO
    $updateQuery = "UPDATE tb_bilhetes
                    SET nome_comprador='$nome_comprador',
                        telefone_comprador='$tel_comprador',
                        dt_reserva='$now',
                        dt_validade='$validade'
                    WHERE numero_bilhete IN ($numeros) AND id_rifa = '$id_rifa'";

    $conn->query($updateQuery);

    if($conn->affected_rows > 0){
        echo true;
    } else {
        echo false;
    }

?>