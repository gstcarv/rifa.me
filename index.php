<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="theme-color" content="#16cfb0">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
      <!-- Bootstrap CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
      <link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,500,800" rel="stylesheet">
      
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

      <!-- Sweet Alert -->
      <link href="/libs/sweetalert/sweetalert.css" rel="stylesheet">
      

      <title>Home • Rife.me</title>
   </head>
   <body>

      <!-- Header -->
      <?php include('includes/PageHeader.php') ?>

      <?php
      
      include('api/conn.php');
      
      $user_id = null;
      
      if(isset($_SESSION['logged_user'])){
         $user_id = $_SESSION['logged_user']['id_anunciante'];
         $user_admin = $_SESSION['logged_user']['admin'];
      }

         $now = date("Y-m-d H:i:s");
         $query = "SELECT img.nome_imagem, rf.*,
                           (SELECT count(*) FROM tb_bilhetes
                           WHERE id_rifa = img.id_rifa 
                           AND (dt_pagamento IS NOT NULL 
                           OR '$now' <= dt_validade)) AS num_bl_reservados,
                           (SELECT count(*) FROM tb_bilhetes
                           WHERE id_rifa = img.id_rifa 
                           AND dt_pagamento IS NOT NULL) AS num_bl_pagos
                        FROM tb_rifas rf
                        INNER JOIN tb_imagens img
                        ON img.id_rifa = rf.id_rifa
                        GROUP BY img.id_rifa
                        ORDER BY img.id_rifa DESC";

      $rifas = $conn->query($query)->fetch_all(MYSQLI_ASSOC);

      ?>
      <!-- Minhas Rifas -->
      <div>
         <div class="container">
            <div class="linha"></div>
            <div class="mt-4 mb-4">
               <h2>Ultimas Rifas</h2>
               <p>As melhores rifas para você!</p>
               <hr>
               <?php if(count($rifas) == 0): ?>
                     <h4>Nenhuma Rifa foi Cadastrada ainda</h4>
                  <?php endif ?>
               <div class="row">
                  <?php foreach($rifas as $rifa): ?>
                     <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="img-thumbnail p-3 mb-3">
                           <a href="/rifa/<?php echo $rifa['id_rifa'] ?>" class="thumbnail-link">
                              <div class="user-rifa-thumb"
                                 style="background-image: url('<?php echo "/imagens/uploads/" . $rifa['nome_imagem'] ?>')">
                                 <?php if($rifa['bilhete_sorteado'] != ""): ?>
                                    <div class="ganhador-container-thumb">
                                       <h4>Ganhador: <?php echo $rifa['nome_ganhador'] ?></h4>
                                       <h4 class="mt-3">Bilhete: <?php echo $rifa['bilhete_sorteado'] ?></h4>
                                    </div>
                                 <?php endif ?>
                              </div>
                           </a>
                           <div class="caption">
                              <h3><?php echo $rifa['titulo'] ?></h3>
                              <hr>
                              <h4 class="text-center">
                                 R$<?php echo number_format($rifa['valor'], 2, ',', '') ?>
                              </h4>
                              <hr>
                              <div class="detalhes-bilhete text-center row">
                                 <p class="col">
                                    <small>Bilhetes</small><br>
                                    <span><?php echo $rifa['num_bilhetes'] ?></span>
                                 </p>
                                 <p class="col">
                                    <small>Reservados</small><br>
                                    <span><?php echo $rifa['num_bl_reservados'] ?></span>
                                 </p>
                                 <p class="col">
                                    <small>Pagos</small><br>
                                    <span><?php echo $rifa['num_bl_pagos'] ?></span>
                                 </p>
                              </div>
                              <hr>
                              <p class="mt-3">
                                 <?php if($user_id && ($rifa['id_anunciante'] == $user_id || $user_admin == true)): ?>
                                    <p class="text-center">
                                       <a href="/rifa/<?php echo $rifa['id_rifa'] ?>/editar">Editar Rifa</a>
                                    </p>
                                 <?php endif ?>
                                 <p class="mt-3">
                                    <a href="/rifa/<?php echo $rifa['id_rifa'] ?>" class="btn btn-block btn-reservar" role="button">Visitar</a>
                                    <?php if($user_id && $rifa['id_anunciante'] == $user_id): ?>
                                       <a href="<?php echo "/perfil/reservas/" . $rifa['id_rifa'] ?>" class="btn btn-block btn-primary" role="button">Reservas</a>
                                    <?php endif ?>
                                 </p>
                              </p>
                           </div>
                        </div>
                     </div>
                  <?php endforeach ?>
               </div>
            </div>
            <div class="linha"></div>
         </div>
      </div>

      <!-- Footer -->
      <?php include('includes/PageFooter.php') ?>

      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      
      <!-- Sweet Alert -->
      <script src="/libs/sweetalert/sweetalert.min.js"></script>
      
      <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
      <script type="text/javascript" src="/libs/bootstrap/js/bootstrap.min.js"></script>

      <?php if(!$user): ?>
         <script type="text/javascript" src="/js/contas.js"></script>
      <?php endif ?>

   </body>
</html>