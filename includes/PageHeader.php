<?php 

   $user = null;

   if (session_status() == PHP_SESSION_NONE) {
      session_start();
   }

   if(isset($_SESSION['logged_user'])){
      $user = $_SESSION['logged_user'];
      include_once('api/conn.php');

      $select = $conn->query("SELECT alterado FROM tb_anunciantes WHERE id_anunciante = '$user[id_anunciante]'");
      $alterado = $select->fetch_array(MYSQLI_ASSOC)['alterado'];
      if($alterado == 1){
         $conn->query("UPDATE tb_anunciantes SET alterado = 0 WHERE id_anunciante = '$user[id_anunciante]'");
         header('location: loggout.php');
      }
   }

?>

<nav class="navbar navbar-expand-lg navbar-light bg-light main-navbar p-4">
  <a class="navbar-brand" href="/">
   <img src="/imagens/logo.png"  width="200px" height="70px" title="Rife.me">
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto"></ul>
    <div class="nav-end">
         <?php if(isset($user)): ?>
            <p class="text-white text-center navbar-user-name">
               <i class="fa fa-user mr-1"></i>
               <span><?php echo $user['nome'] ?></span>
            </p>
            <div class="text-center">
            <button class="btn btn-minha-conta" onclick="location.href='/perfil'">
               Minha Conta
               <?php if($user['admin'] == true): ?>
                  <span style="font-size: 12px" class="ml-2 badge badge-yellow">ADMIN</span> 
               <?php endif ?>
            </button>
            <button class="btn btn-sair" onclick="location.href='/loggout.php'">Sair</button>
            </div>
         <?php else: ?>
               <button class="btn" data-toggle="modal" data-target="#modalLogin">Login</button>
         <?php endif ?>
      </div>
    </div>
  </div>
</nav>

<?php if($user != null): ?>
   <?php if($user['admin'] == true): ?>
      <!-- Modal CADASTRO -->
      <div class="modal fade" id="modalCadastro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Cadastro de Usuário</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formCadastraAnunciante" method="POST">
                     <div class="form-group">
                        <label for="fieldNomeUser">Nome Completo</label>
                        <input required maxlength="70" name="nome" type="text" class="form-control" id="fieldNomeUser" placeholder="Nome Completo">
                     </div>
                     <div class="form-group">
                        <label for="fieldTelUser">Telefone</label>
                        <input data-mask="(00) 00000-0000" name="telefone" maxlength="20" type="tel" class="form-control" id="fieldTelUser" placeholder="Telefone">
                     </div>
                     <div class="form-group">
                        <label for="fieldMailUser">Email</label>
                        <input name="email" maxlength="70" type="email" class="form-control" id="fieldMailUser" aria-describedby="emailHelp" placeholder="Insira o E-mail do Usuário">
                     </div>
                     <div class="form-group">
                        <label for="fieldSenhaUser">Senha</label>
                        <input name="senha" maxlength="60" type="password" class="form-control" id="fieldSenhaUser" placeholder="Senha">
                     </div>
                     <button class="btn btn-primary" id="btnCadastraAnuncianteNovo">Cadastrar</button>
                  </form>
               </div>
            </div>
         </div>
      </div>
   <?php endif ?>
<?php else: ?>
   <!-- Modal ENTRAR-->
   <div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">Login</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <form id="formLogin" method="POST">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Email</label>
                     <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Insira o seu E-mail">
                     <small id="emailHelp" class="form-text text-muted">Nós nunca vamos compartilhar seu e-mail com mais ninguém</small>
                  </div>
                  <div class="form-group">
                     <label for="exampleInputPassword1">Senha</label>
                     <input name="senha" type="password" class="form-control" id="exampleInputPassword1" placeholder="Senha">
                  </div>
                  <div class="custom-control custom-checkbox mr-sm-2">
                     <input name="chbLembrar" type="checkbox" type="checkbox" class="custom-control-input" id="lembrar">
                     <label class="custom-control-label" for="lembrar">Lembrar</label>
                  </div>
                  <br>
                  <button id="btnLogin" class="btn btn-primary">Entrar</button>
               </form>
            </div>
         </div>
      </div>
   </div>
<?php endif ?>