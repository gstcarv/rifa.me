<!-- F0OTER -->
<div class="footer">
   <div class="container">
      <p>Desapegue de forma divertida !</p>
      <div class="redes">
         <div class="row">
            <div class="col-4">
               <div class="icon">
                  <span class="fab fa-facebook"></span>
               </div>
            </div>
            <div class="col-4">
               <div class="icon">
                  <span class="fab fa-instagram"></span>
               </div>
            </div>
            <div class="col-4">
               <div class="icon">
                  <span class="fab fa-whatsapp"></span>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- FIM FOOTER -->