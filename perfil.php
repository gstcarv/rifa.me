<?php

session_start();

if(!isset($_SESSION['logged_user'])){
    header('location: /');
}

$atualizado = null;
if(isset($_SESSION['atualizado'])){
   $atualizado = $_SESSION['atualizado'];
   $_SESSION['atualizado'] = '';
}

include('api/conn.php');

?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <!-- Required meta tags -->
      <meta name="theme-color" content="#16cfb0">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
      <!-- Bootstrap CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
      <link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,500,800" rel="stylesheet">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
      <!-- Sweet Alert -->
      <link href="/libs/sweetalert/sweetalert.css" rel="stylesheet">
      <title>Meu Perfil • Rife.me</title>
   </head>
   <body>
      <!-- Header -->
      <?php include('includes/PageHeader.php') ?>
      <div class="container mt-4">
         <div class="jumbotron">
            <?php if($atualizado == 'rede-social'): ?>
               <div class="alert alert-success" role="alert">
                  As Redes Sociais foram Atualizadas
               </div>
            <?php endif ?>
            <?php if($atualizado == 'perfil'): ?>
               <div class="alert alert-success" role="alert">
                  O Perfil foi atualizado com Sucesso
               </div>
            <?php endif ?>
            <h3 class="mb-4 d-flex align-items-center">
               <i class="fa fa-user mr-2"></i> <?php echo $user['nome'] ?>
               <?php if($user['admin'] == true): ?>
                  <span style="font-size: 12px" class="ml-2 badge badge-warning">ADMIN</span>   
               <?php endif ?>
            </h3>
            <span >
            <i class="fa fa-phone"></i> 
            <span data-mask="(00) 00000-0000"><?php echo $user['telefone'] ?></span>
            </span>
            <br>
            <span>
            <i class="fa fa-envelope"></i> 
            <?php echo $user['email'] ?>
            </span>
            <hr>
            
            <?php if($user['whatsapp'] != ''): ?>
               <a target="_blank" href="https://wa.me/55<?php echo $user['whatsapp'] ?>" class="link-social">
                  <i class="fab fa-whatsapp"></i>
               </a>
            <?php endif ?>

            <?php if($user['facebook'] != ''): ?>
               <a target="_blank" href="<?php echo $user['facebook'] ?>" class="link-social">
                  <i class="fab fa-facebook"></i>
               </a>
            <?php endif ?>

            <?php if($user['instagram'] != ''): ?>
               <a target="_blank" href="<?php echo $user['instagram'] ?>" class="link-social">
                  <i class="fab fa-instagram"></i>
               </a>
            <?php endif ?>

         </div>
         <button class="btn" data-toggle="modal" data-target="#modalEditarPerfil">Editar Perfil</button>
         <button class="btn btn-reservar" data-toggle="modal" data-target="#modalRedesSociais">Redes Sociais</button>
         <hr>
      </div>
      <br>

      <?php if($user['admin'] == true): ?>
      <div class="jumbotron container">
         <span style="font-size: 12px" class="ml-2 badge badge-warning">ÁREA DO ADMIN</span>
         <div class="container mt-4">
            <h2>Usuários</h2>
            <p>Todos os Usuários Cadastrados</p>
            <button class="btn btn-reservar mb-2" data-toggle="modal" data-target="#modalCadastro">Cadastrar novo Usuário</button>
            <div class="table-responsive">
               <table class="table table-striped table-hover text-center tb-anunciantes">
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Permissão</th>
                        <th>Rifas</th>
                        <th>Limite de Rifas</th>
                        <th>Editar</th>
                        <th>Apagar</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                        $select = "SELECT an.*, (
                           SELECT count(*) FROM tb_rifas
                           WHERE id_anunciante = an.id_anunciante 
                        ) AS 'num_rifas_an' 
                        FROM tb_anunciantes an WHERE admin='false'";

                        $allAnunciantes = $conn->query($select)->fetch_all(MYSQLI_ASSOC);

                        echo "<script>var allAnunciantes = " . json_encode($allAnunciantes) . "</script>";

                        foreach($allAnunciantes as $anunciante):                                
                     ?>
                        <tr>
                           <td>
                              <b><?php echo $anunciante['id_anunciante'] ?></b>
                           </td>
                           <td >
                              <?php echo $anunciante['nome'] ?>
                           </td>
                           <td>
                              <?php echo $anunciante['email'] ?>
                           </td>
                           <td>
                              <?php echo $anunciante['tem_permissao'] == 1 ? 'Sim' : 'Não' ?>
                           </td>
                           <td>
                              <?php echo $anunciante['num_rifas_an'] ?>
                           </td>
                           <td>
                              <?php echo $anunciante['max_rifas'] ?>
                           </td>
                           <td>
                              <button class="btn btn-reservar btn-edita-anunciante" data-id="<?php echo $anunciante['id_anunciante'] ?>">Editar</button>
                           </td>
                           <td>
                              <button class="btn btn-sair btn-apaga-anunciante" 
                                 data-id="<?php echo $anunciante['id_anunciante'] ?>"
                                 data-nome="<?php echo $anunciante['nome'] ?>">Apagar</button>
                           </td>
                        </tr>
                     <?php endforeach ?>
                  </tbody>
               </table>
               <?php if(count($allAnunciantes) == 0): ?>
                  <p class="text-center">Nenhum Anunciante Cadastrado</p>
               <?php endif ?>
            </div>
         </div>
         <br>
         <div class="container">
            <h2>Rifas</h2>
            <p>Todas as Rifas Cadastradas</p>
            <div class="table-responsive">
               <table class="table table-striped table-hover text-center tb-anunciantes">
                  <thead>
                     <tr>
                        <th>ID</th>
                        <th>Imagem</th>
                        <th>Titulo</th>
                        <th>Valor</th>
                        <th>Data de Criação</th>
                        <th>Bilhetes</th>
                        <th>Reservados</th>
                        <th>Disponíveis</th>
                        <th>Status</th>
                        <th>Editar</th>
                     </tr>
                  </thead>
                  <tbody>
                   <?php
                        $now = date("Y-m-d H:i:s");               
                        $user_id = $_SESSION['logged_user']['id_anunciante'];

                        $query = "SELECT img.nome_imagem, rf.*,
                                          (SELECT count(*) FROM tb_bilhetes
                                             WHERE id_rifa = img.id_rifa 
                                             AND (dt_pagamento IS NOT NULL 
                                             OR '$now' <= dt_validade)) AS num_bl_reservados
                                       FROM tb_rifas rf
                                       INNER JOIN tb_imagens img
                                       ON img.id_rifa = rf.id_rifa
                                       GROUP BY img.id_rifa
                                       ORDER BY img.id_imagem";

                        $allRifas = $conn->query($query)->fetch_all(MYSQLI_ASSOC);
                        foreach($allRifas as $rifa):
                     ?>
                        <tr>
                           <td>
                              <b><?php echo $rifa['id_rifa'] ?></b>
                           </td>
                           <td>
                              <img class="img-fluid"
                                    width="70"
                                    src="/imagens/uploads/thumb_<?php echo $rifa['nome_imagem'] ?>" />
                           </td>
                           <td>
                              <?php echo $rifa['titulo'] ?>
                           </td>
                           <td>
                              R$ <?php echo number_format($rifa['valor'], 2, ',', '') ?>
                           </td>
                           <td>
                              <?php echo date_format(date_create($rifa['dt_criacao']), 'd/m/Y') ?>
                           </td>
                           <td>
                              <?php echo $rifa['num_bilhetes'] ?>
                           </td>
                           <td>
                              <?php echo $rifa['num_bl_reservados'] ?>
                           </td>
                           <td>
                              <?php echo $rifa['num_bilhetes'] - $rifa['num_bl_reservados'] ?>
                           </td>
                           <td>
                              <?php echo $rifa['bilhete_sorteado'] == "" ? 'Aberta' : 'Finalizada' ?>
                           </td>
                           <td>
                              <a class="btn btn-reservar" href="/rifa/<?php echo $rifa['id_rifa'] ?>/editar" target="_blank">Editar</a>
                           </td>
                        </tr>
                     <?php endforeach ?>
                  </tbody>
               </table>
               <?php if(count($allRifas) == 0): ?>
                  <p class="text-center">Nenhuma Rifa Cadastrada</p>
               <?php endif ?>
            </div>
         </div>
      </div>
      <?php endif ?>

      <?php
      
         $query = "SELECT b.*, tba.* FROM tb_banco_anunciante tba
                  INNER JOIN tb_bancos b
                  ON b.id_banco = tba.id_banco
                  WHERE id_anunciante = '$user[id_anunciante]'";

         $bancos = $conn->query($query)->fetch_all(MYSQLI_ASSOC);

      ?>
      <!-- Meus Bancos -->
      <div>
         <div class="container">
            <hr>
            <div class="mt-4 mb-4">
               <h2>Meus Bancos</h2>
               <p>Esses bancos aparecerão nas suas Rifas</p>
               <div class="row align-items-center" id="containerBancos">
                  <?php foreach($bancos as $banco): ?>
                     <div class="col-xs-12 col-sm-6 col-lg-3 text-center" data-id-banco="<?php echo $banco['id_banco_anunciante'] ?>">
                        <div class="img-thumbnail p-3 mb-2">
                           <div>
                              <img class="img-fluid" src="<?php echo "/imagens/bancos/" . $banco['imagem_banco'] ?>" width="100" alt="Logo Banco">
                           </div>
                           <div class="caption">
                              <br>
                              <h3><?php echo $banco['nome_banco'] ?></h3>
                              <span><b>Titular</b>: <?php echo $banco['titular'] ?></span><br><br>
                              <span><b>CPF</b>: <?php echo $banco['cpf'] ?></span><br>
                              <span><b>Agência</b>: <?php echo $banco['agencia'] ?></span><br>
                              <span><b>Conta</b>: <?php echo $banco['conta'] ?></span><br>
                              <p class="mt-3">
                                 <button target="_blank" class="btn btn-reservar btn-block btn-apagar-banco" data-id-banco="<?php echo $banco['id_banco_anunciante'] ?>">
                                    <span class="fa fa-trash"></span>
                                 </button>
                              </p>
                           </div>
                        </div>
                     </div>
                  <?php endforeach ?>
                  <?php if(count($bancos) == 0): ?>
                     <div class="alert alert-dark m-auto" role="alert">
                        Você ainda não cadastrou nenhum banco!
                     </div>
                  <?php endif ?>
               </div>
               <hr>
               <button class="btn btn-reservar" data-toggle="modal" data-target="#modalNovoBanco">Adicionar Novo Banco</button>
            </div>
         </div>
      </div>

      <?php

         $now = date("Y-m-d H:i:s");               
         $user_id = $_SESSION['logged_user']['id_anunciante'];

         $query = "SELECT img.nome_imagem, rf.*,
                           (SELECT count(*) FROM tb_bilhetes
                              WHERE id_rifa = img.id_rifa 
                              AND (dt_pagamento IS NOT NULL 
                              OR '$now' <= dt_validade)) AS num_bl_reservados,
                           (SELECT count(*) FROM tb_bilhetes
                              WHERE id_rifa = img.id_rifa 
                              AND dt_pagamento IS NOT NULL) AS num_bl_pagos
                        FROM tb_rifas rf
                        INNER JOIN tb_imagens img
                        ON img.id_rifa = rf.id_rifa
                        WHERE id_anunciante='$user_id'
                        GROUP BY img.id_rifa
                        ORDER BY img.id_imagem";

         $rifas = $conn->query($query)->fetch_all(MYSQLI_ASSOC);

      ?>


      <!-- Minhas Rifas -->
      <div>
         <div class="container">
            <div class="linha"></div>
            <div class="mt-4 mb-4">
               <h2>Minhas Rifas</h2>
               <p>Essas são as rifas criadas por você:</p>
               <p>Número máximo de Rifas: <b><?php echo $user['admin'] == 1 ? "Ilimitado" : $user['max_rifas'] ?></b></p>
               <hr>
               <div class="row">
                  <?php foreach($rifas as $rifa): ?>
                     <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="img-thumbnail p-3 mb-3">
                           <?php if($user_id && $rifa['id_anunciante'] == $user_id): ?>
                                 <a href="<?php echo "/perfil/reservas/" . $rifa['id_rifa'] ?>" class="btn btn-block btn-primary mb-2" role="button">Reservas</a>
                           <?php endif ?>
                           <a href="/rifa/<?php echo $rifa['id_rifa'] ?>">
                              <div class="user-rifa-thumb"
                                 style="background-image: url('<?php echo "/imagens/uploads/" . $rifa['nome_imagem'] ?>')"></div>
                           </a>
                           <div class="caption">
                              <h3><?php echo $rifa['titulo'] ?></h3>
                              <hr>
                              <h4 class="text-center">
                                 R$<?php echo number_format($rifa['valor'], 2, ',', '') ?>
                              </h4>
                              <hr>
                              <div class="detalhes-bilhete text-center row">
                                 <p class="col">
                                    <small>Bilhetes</small><br>
                                    <span><?php echo $rifa['num_bilhetes'] ?></span>
                                 </p>
                                 <p class="col">
                                    <small>Reservados</small><br>
                                    <span><?php echo $rifa['num_bl_reservados'] ?></span>
                                 </p>
                                 <p class="col">
                                    <small>Pagos</small><br>
                                    <span><?php echo $rifa['num_bl_pagos'] ?></span>
                                 </p>
                              </div>
                              <hr>
                              <p class="mt-3">
                                 <?php if($user_id && $rifa['id_anunciante'] == $user_id): ?>
                                    <p class="text-center">
                                       <a href="/rifa/<?php echo $rifa['id_rifa'] ?>/editar">Editar Rifa</a>
                                    </p>
                                 <?php endif ?>
                                 <p class="mt-3">
                                    <a href="/rifa/<?php echo $rifa['id_rifa'] ?>" class="btn btn-block btn-reservar" role="button">Visitar</a>
                                    <?php if($user_id && $rifa['id_anunciante'] == $user_id): ?>
                                       <?php if($rifa['bilhete_sorteado'] != ""): ?>
                                          <p class="text-center mt-2">
                                             Rifa Finalizada <b><?php echo date_format(date_create($rifa['dt_finalizacao']), 'd/m/Y H:i') ?></b>
                                             <br>
                                             Ganhador: <b><?php echo $rifa['nome_ganhador'] ?></b>
                                             <br>
                                             Telefone: <b><?php echo $rifa['tel_ganhador']?></b>
                                             <br>
                                             Bilhete: <b><?php echo $rifa['bilhete_sorteado']?></b>
                                          </p>
                                       <?php else: ?>
                                          <button class="btn btn-block btn-reservar btn-modal-finaliza-rifa" role="button"
                                                data-id-rifa="<?php echo $rifa['id_rifa'] ?>"
                                                data-valor-total="<?php echo $rifa['num_bl_pagos'] * $rifa['valor'] ?>"
                                                data-id-anunciante="<?php echo $rifa['id_anunciante'] ?>">Finalizar Rifa</button>
                                       <?php endif ?>
                                    <?php endif ?>
                                 </p>
                              </p>
                           </div>
                        </div>
                     </div>
                  <?php endforeach ?>
                  <?php if(count($rifas) == 0): ?>
                     <div class="alert alert-dark" role="alert">
                        Você ainda não cadastrou nenhuma rifa. Cadastre agora!
                     </div>
                  <?php endif ?>
               </div>
               <hr>
               
               <?php if(count($rifas) >= $user['max_rifas'] && $user['admin'] == 0): ?>
                  <div class="alert alert-dark text-center" role="alert">
                     Você atingiu o seu limite de Rifas. Fale com o Administrador
                  </div>
               <?php else: ?>
                  <button class="btn btn-reservar" data-toggle="modal" data-target="#modalNovaRifa">Criar Nova Rifa</button>
               <?php endif ?>
            
            </div>
            <div class="linha"></div>
         </div>
      </div>

       <!-- Modal Novo Banco -->
       <div class="modal fade" id="modalNovoBanco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Adicionar Novo Banco</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <?php  
               
                  $bancos = $conn->query("SELECT * FROM tb_bancos")->fetch_all(MYSQLI_ASSOC);
               
               ?>
               <div class="modal-body">
                  <form id="formNovoBanco" method="POST">
                     <div class="form-group">
                        <label>Banco</label>
                        <select name="id_banco" class="form-control">
                           <?php foreach($bancos as $banco): ?>
                              <option value="<?php echo $banco['id_banco'] ?>">
                                 <?php echo $banco['nome_banco'] ?>
                              </option>
                           <?php endforeach ?>
                        </select>
                     </div>
                     <div class="form-group">
                        <label for="fieldTitularBanco">Titular</label>
                        <input value="<?php echo $user['nome'] ?>" maxlength="70" name="titular_banco" type="text" class="form-control" id="fieldTitularBanco" placeholder="Digite o Titular">
                     </div>
                     <div class="form-group">
                        <label for="fieldCpfBanco">CPF</label>
                        <input maxlength="20" name="cpf_banco" type="text" class="form-control" id="fieldCpfBanco" placeholder="Digite o CPF"
                        data-mask="000.000.000-00">
                     </div>
                     <div class="form-group">
                        <label for="fieldAgenciaBanco">Agência</label>
                        <input maxlength="10" name="agencia_banco" type="text" class="form-control" id="fieldAgenciaBanco" placeholder="Digite a Agência ">
                     </div>
                     <div class="form-group">
                        <label for="fieldContaBanco">Conta</label>
                        <input maxlength="10" name="conta_banco" type="text" class="form-control" id="fieldContaBanco" placeholder="Digite a Conta ">
                     </div>
                     <button class="btn btn-primary" id="btnAdicionarNovoBanco">Adicionar Banco</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal Nova Rifa-->
      <div class="modal fade" id="modalNovaRifa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Criar Nova Rifa</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formNovaRifa" method="POST" enctype='multipart/form-data' onsubmit="return false">
                     <div class="form-group">
                        <label for="tituloRifa">Titulo da Rifa</label>
                        <input name="titulo" maxlength="70" type="text" class="form-control" id="tituloRifa" aria-describedby="emailHelp" placeholder="Insira o Titulo da Rifa">
                     </div>
                     <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="valorRifa">Valor da Rifa</label>
                                <input name="valor" type="number" min="1" class="form-control" id="valorRifa" placeholder="Insira o Valor da Rifa">
                            </div>
                            <div class="col-md-6">
                                <label for="numBilhetes">Número de Bilhetes</label>
                                <input name="num_bilhetes" type="number" min="1" class="form-control" id="numBilhetes" placeholder="Insira o Número de Bilhetes">
                            </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="descricaoRifa">Descrição / Regulamento</label>
                        <textarea name="descricao_rifa" id="descricaoRifa" cols="30" rows="10" class="form-control" placeholder="Insira a Descrição ou o Regulamento"></textarea>
                     </div>
                     <br>
                     <span>Imagens</span><br>
                     <small class="text-muted">Clique no quadrado para selecionar</small>
                     <div id="containerImagens" class="row mt-2">
                        <label class="rifa-thumbnail" for="img1">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_1" id="img1" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img2">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_2" id="img2" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img3">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_3" id="img3" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img4">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_4" id="img4" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img5">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_5" id="img5" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img6">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_6" id="img6" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img7">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_7" id="img7" accept="image/*">
                        </label>
                        <label class="rifa-thumbnail" for="img8">
                            <span class="fa fa-camera"></span>
                            <input type="file" class="hide" name="image_8" id="img8" accept="image/*">
                        </label>
                     </div>
                     <br>
                     <div class="progress mb-2" style="height: 20px">
                        <div class="progress-bar progress-bar-striped active bg-info" role="progressbar" id="uploadProgressBar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <button class="btn btn-reservar btn-block" id="btnCriarRifa">Criar Rifa</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal Editar Perfil -->
      <div class="modal fade" id="modalEditarPerfil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar Perfil</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formEditarPerfil" action="/api/edita_perfil.php" method="POST">
                     <div class="form-group">
                        <label for="fieldNome">Nome Completo</label>
                        <input maxlength="70" value="<?php echo $user['nome'] ?>" name="nome" type="text" class="form-control" id="fieldNome" placeholder="Nome Completo">
                     </div>
                     <div class="form-group">
                        <label for="fieldTel">Telefone</label>
                        <input data-mask="(00) 00000-0000" maxlength="20" value="<?php echo $user['telefone'] ?>" name="telefone" type="tel" class="form-control" id="fieldTel" placeholder="Telefone">
                     </div>
                     <div class="form-group">
                        <label for="fieldMail">Email</label>
                        <input value="<?php echo $user['email'] ?>" disabled type="text" class="form-control" id="fieldMail" aria-describedby="emailHelp" placeholder="Insira o seu E-mail">
                        <small id="emailHelp" class="form-text text-muted">Nós nunca vamos compartilhar seu e-mail com mais ninguém</small>
                     </div>
                     <div class="form-group">
                        <label for="fieldSenha">Nova Senha</label>
                        <input maxlength="60" name="senha" type="password" class="form-control" id="fieldSenha" placeholder="Senha">
                        <small id="emailHelp" class="form-text text-muted">Se não deseja alterar a senha, deixa-a vazia</small>
                     </div>
                     <button class="btn btn-primary" id="btnEditaPerfil">Salvar Alterações</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

       <!-- Modal Editar Ususário -->
       <div class="modal fade" id="modalEditarAnunciante" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Editar Anunciante</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formEditarUsuario" action="/api/edita_perfil.php" method="POST">
                     <div class="form-group">
                        <label for="fieldNewUserName">Nome Completo</label>
                        <input maxlength="70" name="nome" type="text" class="form-control" id="fieldNewUserName" placeholder="Nome Completo">
                     </div>
                     <div class="form-group">
                        <label for="fieldNewUserTel">Telefone</label>
                        <input data-mask="(00) 00000-0000" maxlength="20" name="telefone" type="tel" class="form-control" id="fieldNewUserTel" placeholder="Telefone">
                     </div>
                     <div class="form-group">
                        <label for="fieldNewUserMail">Email</label>
                        <input disabled type="text" class="form-control" id="fieldNewUserMail" aria-describedby="emailHelp" placeholder="Insira o seu E-mail">
                        <small id="emailHelp" class="form-text text-muted">Nós nunca vamos compartilhar seu e-mail com mais ninguém</small>
                     </div>
                     <div class="form-group">
                        <label for="fieldNewUserPassword">Nova Senha</label>
                        <input maxlength="60" name="senha" type="password" class="form-control" id="fieldNewUserPassword" placeholder="Senha">
                        <small id="emailHelp" class="form-text text-muted">Se não deseja alterar a senha, deixa-a vazia</small>
                     </div>
                     <div class="form-group">
                        <label for="fieldMaxRifas">Número máximo de Rifas</label>
                        <input name="max_rifas" type="number" class="form-control" min="0" id="fieldMaxRifas" placeholder="Digite o Número Maximo de Rifas">
                     </div>
                     <div class="custom-control custom-checkbox mr-sm-2">
                        <input name="chbPermissao" type="checkbox" type="checkbox" class="custom-control-input" id="chbPermissaoLogin">
                        <label class="custom-control-label" for="chbPermissaoLogin">Permissão de Login</label>
                     </div>
                     <br>
                     <button class="btn btn-primary" id="btnEditaAnunciante">Salvar Alterações</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal Editar Redes Sociais -->
      <div class="modal fade" id="modalRedesSociais" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Redes Sociais</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formRedesSociais" action="/api/edita_rede_social.php" method="POST">
                     <div class="form-group">
                        <label for="wpp">Whatsapp</label>
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">
                                 <i class="fab fa-whatsapp"></i>
                              </span>
                           </div>
                           <input maxlength="300" value="<?php echo $user['whatsapp'] ?>" name="user_whatsapp" type="text" class="form-control" id="whatsapp" placeholder="Digite o Número do Whatsapp com DDD" aria-describedby="validationTooltipUsernamePrepend">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="fb">Facebook</label>
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">
                                 <i class="fab fa-facebook"></i>
                              </span>
                           </div>
                           <input maxlength="300" value="<?php echo $user['facebook'] ?>" name="user_facebook" type="text" class="form-control" id="fb" placeholder="Insira o link do seu Perfil no Facebook" aria-describedby="validationTooltipUsernamePrepend">
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="insta">Instagram</label>
                        <div class="input-group">
                           <div class="input-group-prepend">
                              <span class="input-group-text">
                                 <i class="fab fa-instagram"></i>
                              </span>
                           </div>
                           <input maxlength="300" value="<?php echo $user['instagram'] ?>" name="user_instagram" type="text" class="form-control" id="insta" placeholder="Insira o Link do seu Perfil no Instagram" aria-describedby="validationTooltipUsernamePrepend">
                        </div>
                     </div>
                     <input type="hidden" name="action" value="edit_redes_sociais">
                     <input type="submit" class="btn btn-primary" value="Enviar" />
                  </form>
               </div>
            </div>
         </div>
      </div>

      <!-- Modal Finalizar Rifa -->
      <div class="modal fade" id="modalFinalizarRifa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Finalizar Rifa</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form id="formFinalizarRifa" method="POST">
                     <h6>Total Pago: <span id="totalRifa"></span></h6>
                     <hr>
                     <div class="form-group">
                        <label for="nomeGanhador">Nome do Ganhador</label>
                        <input type="text" maxlength="70" id="nomeGanhador" class="form-control" placeholder="Digite o Nome do Ganhador">
                     </div>
                     <div class="form-group">
                        <label for="telefoneGanhador">Telefone do Ganhador</label>
                        <input type="text" maxlength="20" id="telefoneGanhador" class="form-control" placeholder="Digite o Telefone do Ganhador">
                     </div>
                     <div class="form-group">
                        <label for="bilheteSorteado">Bilhete Sorteado</label>
                        <input type="number" id="bilheteSorteado" class="form-control" placeholder="Digite o Bilhete Sorteado">
                     </div>
                     <button class="btn btn-reservar" id="btnFinalizarRifa">Finalizar Rifa</button>
                  </form>
               </div>
            </div>
         </div>
      </div>

      <!-- Footer -->
      <?php include('includes/PageFooter.php') ?>
    
      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <!-- Jquery Mask -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
      <!-- Sweet Alert -->
      <script src="/libs/sweetalert/sweetalert.min.js"></script>
      <!-- compressor js -->
      <script src="/libs/compressor/image-compressor.min.js"></script>
      <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
      <script type="text/javascript" src="/libs/bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="/js/perfil.js"></script>
      <?php if($user['admin'] == true): ?>
         <script type="text/javascript" src="/js/admin.js"></script>
      <?php endif ?>
   </body>
</html>