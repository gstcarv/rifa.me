<?php

   include('api/conn.php');

   // PEGA A RIFA DO BANCO
   $id_rifa = $_GET['id_rifa'];

   $now = date("Y-m-d H:i:s");
   $select = "SELECT rf.*,
                     an.nome as nome_an,
                     an.telefone as tel_an,
                     an.whatsapp as whatsapp,
                     an.facebook as facebook,
                     an.instagram as instagram
              FROM tb_rifas rf
              INNER JOIN tb_anunciantes an
               ON rf.id_anunciante = an.id_anunciante
              WHERE rf.id_rifa='$id_rifa'";
   
   $info_rifa = $conn->query($select)->fetch_array(MYSQLI_ASSOC);

   if(!$info_rifa){
      header('location: /');
   }

?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <!-- Required meta tags -->
      <meta name="theme-color" content="#16cfb0">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
      <!-- Bootstrap CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
      <link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,500,800" rel="stylesheet">
      
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

      <!-- Sweet Alert -->
      <link href="/libs/sweetalert/sweetalert.css" rel="stylesheet">

      <title><?php echo $info_rifa['titulo'] ?> • Rife.me </title>
   </head>
   <body>

      <!-- Header -->
      <?php include('includes/PageHeader.php') ?>

      <?php
            
         // PEGA AS IMAGENS
         $queryImagens = $conn->query("SELECT nome_imagem FROM tb_imagens WHERE id_rifa='$id_rifa' ORDER BY id_imagem");
         $imagens = $queryImagens->fetch_all(MYSQLI_ASSOC);

         $mainImage = $imagens[0]['nome_imagem'];
        
      ?>
      
      <!-- CORPO ANUNCIO -->
      <div class="corpo_anuncio">
         <div class="container">
            <h1><?php echo $info_rifa['titulo'] ?></h1>
            <?php if($info_rifa['bilhete_sorteado'] != ""): ?>
               <div class="alert alert-danger">
                  Essa Rifa Já foi Finalizada<br>
                  Data de Finalização: <?php echo date_format(date_create($info_rifa['dt_finalizacao']), 'd/m/Y H:s')?>
               </div>
            <?php endif ?>
            <p>Criado <?php echo date_format(date_create($info_rifa['dt_criacao']), 'd/m/Y'); ?></p>
            <div class="row">
               <div class="col-md-8 mb-2">
                  <img src="<?php echo "/imagens/uploads/$mainImage" ?>" class="img-fluid" id="troca_imagem">
               </div>
               <div class="col-md-4">
                  <div class="corpo_preco">
                     <h2>R$ <?php echo number_format($info_rifa['valor'], 2, ',', ''); ?></h2>
                  </div>
                  <div class="corpo_anunciante mb-3">
                     <p><b>Anunciante</b></p>
                     <p><?php echo $info_rifa['nome_an'] ?></p>
                     <p><span data-mask="(00) 00000-0000"><?php echo $info_rifa['tel_an'] ?></span></p>
                  </div>
                 <div class="text-center">
                  <div class="share">Redes Sociais</div>
                     <?php if(!$info_rifa['whatsapp'] 
                           && !$info_rifa['whatsapp'] 
                           && !$info_rifa['whatsapp']): 
                     ?>
                        <br>
                        <div class="alert alert-dark">Esse anunciante ainda não cadastrou nenhuma rede social</div>
                     <?php endif ?>
                     <?php if($info_rifa['whatsapp'] != ''): ?>
                        <a target="_blank" href="https://wa.me/55<?php echo $info_rifa['whatsapp'] ?>" class="link-social">
                           <i class="fab fa-whatsapp"></i>
                        </a>
                     <?php endif ?>

                     <?php if($info_rifa['facebook'] != ''): ?>
                        <a target="_blank" href="<?php echo $info_rifa['facebook'] ?>" class="link-social">
                           <i class="fab fa-facebook"></i>
                        </a>
                     <?php endif ?>

                     <?php if($info_rifa['instagram'] != ''): ?>
                        <a target="_blank" href="https://www.instagram.com/<?php echo $info_rifa['instagram'] ?>" class="link-social">
                           <i class="fab fa-instagram"></i>
                        </a>
                     <?php endif ?>
                 </div>
               </div>
            </div>
            <div class="fundo_miniatura">
               <?php 
                  foreach($imagens as $imagem):
                     $filename = $imagem['nome_imagem'];
                     $thumb = "/imagens/uploads/thumb_$filename";
                     $path = "/imagens/uploads/$filename";

               ?>
                  <div class="img-thumbnail img_miniatura"
                       style="background-image: url('<?php echo $thumb ?>')"
                       data-thumb-for="<?php echo $path ?>"></div>   
               <?php endforeach ?>
            </div>
         </div>
      </div>
      <!-- FIM CORPO ANUNCIO -->
      <br>
      <br>
      <!-- DESCRIÇÃO -->
      <div class="descricao">
         <div class="container">
            <div class="linha"></div>
            <h2>Descrição</h2>
            <p><?php echo str_replace('\n', PHP_EOL,$info_rifa['descricao']) ?></p>
            <div class="linha"></div>
         </div>
      </div>
      <!-- FIM DESCRIÇÃO -->

      <!-- NÚMEROS-->
      <?php 
         
         $select_bilhetes = "SELECT * FROM
                              tb_bilhetes
                           WHERE id_rifa='$id_rifa'";

         $bilhetes = $conn->query($select_bilhetes)->fetch_all(MYSQLI_ASSOC);
        
         // FUNÇÃO PARA VERIFICAR SE A RIFA FOI PAGA
         function pago($arr){
            if($arr['dt_pagamento']){
               return true;
            } else {
               return false;
            }
         }

         // FILTRA OS BILHETES E PEGA TODAS AS RIFAS PAGAS
         $pagos = array_filter($bilhetes, 'pago');

         // FUNÇÃO PARA VERIFICAR SE A RIFA ESTÁ RESERVADA
         function reservado($arr){
            $now = date("Y-m-d H:i:s");
            if($arr['dt_reserva'] 
               && !$arr['dt_pagamento']
               && $now < $arr['dt_validade'])
            {
               return true;
            } else {
               return false;
            }
         }

         // FILTRA OS BILHETES E PEGA TODAS AS RIFAS RESERVADAS
         $reservados = array_filter($bilhetes, 'reservado');

         $num_bilhetes = count($bilhetes);
         $num_reservados = count($reservados);
         $num_pagos = count($pagos);

         $num_disponiveis = $num_bilhetes - $num_reservados - $num_pagos;

         // VERIFICA SE O USUÁRIO LOGADO É O ANUNCIANTE DA RIFA
         $isDonoRifa = $info_rifa['id_anunciante'] == $user['id_anunciante'];

         // [ANUNCIANTES DA RIFA NÃO PODEM RESERVAR DA PRÓPRIA RIFA]

      ?>

      <div class="numeros">
         <div class="container">
         <?php if($info_rifa['bilhete_sorteado'] == ""): ?>
            <h2 id="teste">Números</h2>
            <p>Reservados: <b><?php echo "$num_reservados/$num_bilhetes" ?></b></p>
            <p>Disponiveis: <b><?php echo $num_disponiveis ?></b></p>
            <p>Pagos: <b><?php echo $num_pagos ?></b></p>
            <div class="row">
               <div class="numero-demo">Disponível</div>
               <div class="numero-demo reserved ml-2 mr-2">Reservado</div>
               <div class="numero-demo sold">Pago</div>
            </div>
         </div>
            <div class="container-fluid mt-5">
               <?php if(!$isDonoRifa): ?>
                  <div class="container">
                     <button class="btn btn-block btn-reservar btn-reserva-numero disabled btn-reserva-fix">Reservar Números</button>
                  </div>
               <?php endif ?>
               <br>
               <div class="row justify-content-center">
                  <?php foreach($bilhetes as $bilhete): ?>
                     <?php
                     
                        if($bilhete['nome_comprador']){
                           if($bilhete['dt_pagamento']){
                              $date = $bilhete['dt_pagamento'];
                           } else {
                              $date = $bilhete['dt_reserva'];
                           }

                           $date = date_format(date_create($date), 'd/m/Y H:i:s');

                           $tooltip = "data-toggle='tooltip' data-html='true' data-placement='top' title='<i>" . $bilhete['nome_comprador'] . "</i><br>$date'";
                        }

                        if(!$bilhete['dt_reserva'] ){
                           echo "<div class='btn_numero col-3 col-md-2 col-lg-1 m-2' data-bilhete='$bilhete[numero_bilhete]'>";
                        } else if ($bilhete['dt_pagamento']){
                           echo "<div class='btn_numero col-3 col-md-2 col-lg-1 m-2 sold' $tooltip>";
                        } else if ($bilhete['dt_reserva']) {
                           $now = date("Y-m-d H:i:s");
                           if($now > $bilhete['dt_validade']){
                              echo "<div class='btn_numero col-3 col-md-2 col-lg-1 m-2' data-bilhete='$bilhete[numero_bilhete]'>";
                           } else {
                              echo "<div class='btn_numero col-3 col-md-2 col-lg-1 m-2 reserved' $tooltip>";
                           }
                        }
                     
                     ?>
                        <?php 
                           echo str_pad($bilhete['numero_bilhete'], 3, '0', STR_PAD_LEFT);
                        ?>
                     </div>
                     <?php endforeach; ?>
               </div>
               <br>
               <?php if(!$isDonoRifa): ?>
                  <div class="container">
                     <button class="btn btn-block btn-reservar btn-reserva-bottom btn-reserva-numero disabled">Reservar Números</button>
                  </div>
               <?php endif ?>
            <?php else: ?>
               <h2 class="text-danger">Resultado da Rifa</h2>
               <br>
               <h4>Finalizada <?php echo date_format(date_create($info_rifa['dt_finalizacao']), 'd/m/Y H:i') ?></h4>
               <h4>Ganhador: <?php echo $info_rifa['nome_ganhador'] ?></h4>
               <h4>Bilhete Sorteado: <?php echo $info_rifa['bilhete_sorteado'] ?></h4>
            <?php endif ?>
            <br>
            <div class="linha"></div>
         </div>
      </div>
      <!-- FIM NÚMEROS -->

      <?php if(!$isDonoRifa): ?>
         <!-- Modal Reserva -->
         <div class="modal fade" id="modalReservaNumero" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
               <div class="modal-content">
                  <div class="modal-header">
                     <h5 class="modal-title" id="exampleModalLabel">Reservar Numero</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                     </button>
                  </div>
                  <div class="modal-body">
                  <form id="formReserva" method="POST">
                     <div class="form-group">
                           <span for="name">Deseja reservar o(s) número(s):</span>
                           <div id="numerosReservados">
                           </div>
                        </div>
                        <div class="form-group">
                           <span for="name">Valor a pagar: </span>
                           <span>R$&nbsp;<span id="valorPagar"></span><br>
                           <span for="name">Por favor, preencha os dados abaixo:</span>
                        </div>
                        <div class="form-group">
                           <span for="name">Nome Completo: </span>
                           <input maxlength="70" type="text" name="nome" id="name" maxlength="100" class="form-control only_character" placeholder="Insira seu nome completo">
                        </div>
                        <div class="form-group">
                           <span for="phone">Telefone:</span>
                           <input maxlength="20" type="text" name="telefone" id="phone" class="form-control phone_mask" placeholder="Insira seu telefone">
                        </div>
                     </form>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn" data-dismiss="modal">Cancelar</button>
                     <button type="button" id="btnEnviaReserva" class="btn btn-reservar">Reservar</button>
                  </div>
               </div>
            </div>
         </div>
      <?php endif ?>

      <?php
      
         $query = "SELECT b.*, tba.* FROM tb_banco_anunciante tba
                  INNER JOIN tb_bancos b
                  ON b.id_banco = tba.id_banco
                  WHERE id_anunciante = '$info_rifa[id_anunciante]'";

         $bancos = $conn->query($query)->fetch_all(MYSQLI_ASSOC);

      ?>
      <!-- RECOMENDAÇÕES-->
      <div class="recomenda">
         <div class="container">
            <h2>Pagamentos</h2>
            <div class="box_anuncios">
            <div class="row align-items-center justify-content-center">
                  <?php foreach($bancos as $banco): ?>
                     <div class="col-12 col-md-6 col-lg-3 text-center">
                        <div class="img-thumbnail p-3 mb-2">
                           <div>
                              <img class="img-fluid" src="<?php echo "/imagens/bancos/" . $banco['imagem_banco'] ?>" width="100" alt="Logo Banco">
                           </div>
                           <div class="caption">
                              <br>
                              <h3><?php echo $banco['nome_banco'] ?></h3>
                              <span><b>Titular</b>: <?php echo $banco['titular'] ?></span><br><br>
                              <span><b>CPF</b>: <?php echo $banco['cpf'] ?></span><br>
                              <span><b>Agência</b>: <?php echo $banco['agencia'] ?></span><br>
                              <span><b>Conta</b>: <?php echo $banco['conta'] ?></span><br>
                           </div>
                        </div>
                     </div>
                  <?php endforeach ?>
                  <?php if(count($bancos) == 0): ?>
                     <div class="alert alert-dark" role="alert">
                        Esse anunciante não cadastrou nenhum banco. Entre em Contato!
                     </div>
                  <?php endif ?>
               </div>
            </div>
         </div>
      </div>
      <!-- FIM RECOMENDAÇÕES -->

      <!-- Footer -->
      <?php include('includes/PageFooter.php') ?>

      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

      <!-- Jquery Mask -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

      <!-- Sweet Alert -->
      <script src="/libs/sweetalert/sweetalert.min.js"></script>
      
      <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
      <script type="text/javascript" src="/libs/bootstrap/js/bootstrap.min.js"></script>
      
      <?php
      
         $valRifa = $info_rifa['valor'];
         echo "<script>var valorRifa = $valRifa</script>"
      
      ?>

      <script type="text/javascript" src="/js/troca_imagem.js"></script>
      <script type="text/javascript" src="/js/numeros.js"></script>
      <?php if(!$user): ?>
         <script type="text/javascript" src="/js/contas.js"></script>
      <?php endif ?>
   </body>
</html>