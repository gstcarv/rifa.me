<?php

   include('api/conn.php');

   // PEGA A RIFA DO BANCO DE DADOS
   $id_rifa = $_GET['id_rifa'];

   $select = "SELECT rf.*,
                     an.nome as nome_an,
                     an.id_anunciante as id_an,
                     an.telefone as tel_an
              FROM tb_rifas rf
              INNER JOIN tb_anunciantes an
               ON rf.id_anunciante = an.id_anunciante
              WHERE rf.id_rifa='$id_rifa'";
   
   $info_rifa = $conn->query($select)->fetch_array(MYSQLI_ASSOC);

   if(!$info_rifa){
      header('location: /');
   }
   
   session_start();
   
   // SE A RIFA NÇAO FOR DO ANNCIANTE OU ANUNCIANTE NÃO FOR ADMINISTRADOR, REDIRECIONA PARA FORA
   $user = $_SESSION['logged_user'];
   
   if(!isset($user)){
      header("location: /rifa/$id_rifa");
   }

   if($user['id_anunciante'] != $info_rifa['id_an'] 
      && $user['admin'] != '1')
   {
         header("location: /rifa/$id_rifa");
   }

?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <!-- Required meta tags -->
      <meta name="theme-color" content="#16cfb0">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
      <!-- Bootstrap CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
      <link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,500,800" rel="stylesheet">
      
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

      <!-- Sweet Alert -->
      <link href="/libs/sweetalert/sweetalert.css" rel="stylesheet">
      

      <title>Editar • Rife.me</title>
   </head>
   <body>

      <!-- Header -->
      <?php include('includes/PageHeader.php') ?>

      <!-- CORPO ANUNCIO -->
      <div class="corpo_anuncio">
         <div class="container">
            <h1><?php echo $info_rifa['titulo'] ?></h1>
            <p>Data de Criação: <?php echo date_format(date_create($info_rifa['dt_criacao']), 'd/m/Y'); ?></p>
            <button class="btn btn-sair" id="btnApagarRifa">Apagar essa rifa</button>
            <hr>
            <h3>Editar</h3>
            
            <form id="formEditarRifa" method="POST" enctype='multipart/form-data' onsubmit="return false">
                     <input value="<?php echo $info_rifa['id_rifa'] ?>" name="id_rifa" type="hidden" id="idRifa">
                     <div class="form-group">
                        <label for="tituloRifa">Titulo da Rifa</label>
                        <input value="<?php echo $info_rifa['titulo'] ?>" name="titulo" maxlength="70" type="text" class="form-control" id="tituloRifa" aria-describedby="emailHelp" placeholder="Insira o Titulo da Rifa">
                     </div>
                     <div class="form-group">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="valorRifa">Valor da Rifa</label>
                                <input disabled value="<?php echo number_format($info_rifa['valor'], 2, '.', '') ?>" type="number" min="1" class="form-control" id="valorRifa" placeholder="Insira o Valor da Rifa">
                            </div>
                            <div class="col-md-6">
                                <label for="numBilhetes">Número de Bilhetes</label>
                                <input disabled value="<?php echo $info_rifa['num_bilhetes'] ?>" type="number" min="1" class="form-control" id="numBilhetes" placeholder="Insira o Número de Bilhetes">
                            </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <label for="descricaoRifa">Descrição / Regulamento</label>
                        <textarea name="descricao_rifa" id="descricaoRifa" cols="30" rows="10" class="form-control" placeholder="Insira a Descrição ou o Regulamento"><?php echo str_replace('<br />', '', $info_rifa['descricao']) ?></textarea>
                     </div>
                     <br>

                     <?php 
                     
                        // PEGA AS IMAGENS PARA EDITAR
                        $queryImagens = $conn->query("SELECT nome_imagem, id_imagem FROM tb_imagens WHERE id_rifa='$id_rifa' ORDER BY id_imagem");
                        $imagens = $queryImagens->fetch_all(MYSQLI_ASSOC);

                        // FUNÇÃO PARA PEGAR IMAGEM A PARTIR DO INDICE
                        function get_image($index){
                           global $imagens;
                              
                           if(isset($imagens[$index]['nome_imagem'])){
                             $path = $imagens[$index]['nome_imagem'];
                             return "url('/imagens/uploads/thumb_$path')";
                           } else {
                              return "none";
                           }
                        }

                        // FUNÇÃO PARA PEGAR ID DA IMAGEM A PARTIR DO INDICE
                        function get_id($index){
                           global $imagens;
                              
                           if(isset($imagens[$index]['id_imagem'])){
                             return $imagens[$index]['id_imagem'];
                           } else {
                              return "";
                           }
                        }
                     
                     ?>
                     <span>Imagens</span><br>
                     <small class="text-muted">Para selecionar uma nova foto, remova a existente</small>
                     <div id="containerImagens" class="row mt-2">
                        <?php
                        
                        for($i = 1; $i <= 8; $i++):
                        
                        // VERIFICA SE TEM IMAGEM NO INDICE DA ARRAY
                        $hasImage = isset($imagens[$i - 1]['nome_imagem']) ? true : false;

                        // SE SIM, ELE RENDERIZA A IMAGEM
                        $pointerEvents = "";
                        $disabled = "";
                        if($hasImage){
                           $pointerEvents = "pointer-events: none";
                           $disabled = "disabled";
                        }
                        
                        ?>
                           <div class="col-s-12 m-2">
                              <label class="rifa-thumbnail" for="<?php echo "img$i" ?>"
                                    style="background-image: <?php echo get_image($i - 1) ?>;
                                           <?php echo $pointerEvents ?>">
                                 <?php if($hasImage == false): ?>
                                    <span class="fa fa-camera"></span>
                                 <?php else: ?>
                                    <span class="fa fa-camera hide"></span>
                                 <?php endif ?>
                                 <input data-id-imagem="<?php echo get_id($i - 1) ?>" type="file" class="hide" name="<?php echo "image_$i" ?>" id="<?php echo "img$i" ?>" accept="image/*" <?php echo $disabled ?>>
                              </label><br>
                              <button class="btn btn-block" onclick="removeFoto('<?php echo 'img' . $i ?>')">Remover Foto</button>
                           </div>

                        <?php endfor ?>
                     </div>
                     <br>
                     <div class="progress mb-2" style="height: 20px">
                        <div class="progress-bar progress-bar-striped active bg-info" role="progressbar" id="uploadProgressBar" style="width: 0%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                     </div>
                     <button class="btn btn-reservar btn-block" id="btnEditarRifa">Enviar Alterações</button>
                  </form>

         </div>
      </div>
      <!-- FIM CORPO ANUNCIO -->
      <br>
      <br>


      <!-- Footer -->
      <?php include('includes/PageFooter.php') ?>

      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

      <!-- Jquery Mask -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>

      <!-- Sweet Alert -->
      <script src="/libs/sweetalert/sweetalert.min.js"></script>

      <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
      <script type="text/javascript" src="/libs/bootstrap/js/bootstrap.min.js"></script>

      <!-- compressor js -->
      <script src="/libs/compressor/image-compressor.min.js"></script>

      <script src="/js/editarRifa.js"></script>
      
      <?php if(!$user): ?>
         <script type="text/javascript" src="/js/contas.js"></script>
      <?php endif ?>

   </body>
</html>