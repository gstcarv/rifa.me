var replaced = []

$('.rifa-thumbnail input').change(function () {
    var file = $(this)[0].files[0]
    var $thumb = $(this).parent();
    if (file) {
        var thumb = URL.createObjectURL(file);
        $thumb.css('background-image', "url(" + thumb + ")");
        $thumb.css('pointer-events', 'none');
        $thumb.find('.fa').addClass('hide');
        $(this).prop('disabled', true);
    } else {
        $thumb.css('background-image', "none");
    }
})

numFiles = 0;
compressed = [];

$("#btnEditarRifa").click(function (e) {
    e.preventDefault();

    var rifa = {
        titulo: $('#tituloRifa').val(),
        valor: $('#valorRifa').val(),
        numBilhetes: $('#numBilhetes').val(),
        descricao: $('#descricaoRifa').val()
    }

    if (rifa.titulo.length < 6) {
        err("Digite um titulo maior");
    } else if (rifa.valor <= 0) {
        err("Insira um valor válido para a Rifa");
    } else if (rifa.numBilhetes <= 0) {
        err("Insira um número válido de bilhetes");
    } else if (rifa.descricao.length <= 20) {
        err("Insira uma Descrição com no mínimo 20 caracteres");
    } else {
        var compressor = new ImageCompressor();
        $('#formEditarRifa input[type="file"]').each(function () {
            var files = $(this)[0].files[0];
            if (files) {
                numFiles++;
                compressor.compress(files, {
                    quality: .2,
                    success: function(res) {
                        compressed.push(res);
                        send(rifa);
                    }
                })
            }
        })


        if($("input[type='file'][disabled]").length == 0 && numFiles == 0){
            err("Selecione pelo menos uma imagem");
        } else {
            $(this).css('pointer-events', 'none')
            .html('<span class="fa fa-spinner fa-spin"></span> Enviando...');
            if(numFiles == 0) send(rifa);
        }


    }

    return false;
});

function send(rifa) {
    if (compressed.length == numFiles) {
        var data = new FormData($("#formEditarRifa"));

        var index = 1;
        compressed.forEach(function (file) {
            data.append('imagem_' + index, file)
            index++;
        })

        data.append('titulo', rifa.titulo);
        data.append('descricao', rifa.descricao);
        data.append('id_rifa', $('#idRifa').val());
        data.append('descricao', rifa.descricao);
        data.append('replaced_images', JSON.stringify(replaced));

        $("#uploadProgressBar").text("0%");

        $.ajax({
            type: "POST",
            url: "/api/edita_rifa.php",
            data: data,
            contentType: false,
            processData: false,
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        var progressString = percentComplete + '%'
                        $("#uploadProgressBar").css('width', progressString)
                            .text(progressString);
                    }
                }, false);

                return xhr;
            },
            success: function (r) {
                location.href = "/rifa/" + $('#idRifa').val();
            }
        });
        return false;
    }
}

function err(msg) {
    swal("Campo Incorreto", msg, 'error');
}

function removeFoto(id){
    var element = $("#" + id);
    element.parent().css({
        backgroundImage: 'none',
        pointerEvents: 'initial'
    });
    element.parent().find('.fa').removeClass('hide')
    element.val();
    element.prop('disabled', false);
    if(element.attr('data-id-imagem')){
        replaced.push(element.attr('data-id-imagem'))
        element.attr('data-id-imagem', '');
    }
}

$("#btnApagarRifa").click(function (e) { 
    e.preventDefault();
    swal({
        title: 'Apagar Rifa',
        text: 'Deseja realmente apagar essa rifa?',
        showCancelButton: true,
        type: 'warning'
    }).then(function(r){
        if(r.value == true){
            $.post('/api/apaga_rifa.php', {
                id_rifa: $('#idRifa').val()
            }).done(function(r){
                location.href = "/perfil";
            })
        }
    })
});