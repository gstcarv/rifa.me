Number.prototype.pad = function (n) {
	return new Array(n).join('0').slice((n || 2) * -1) + this;
}

var links = document.querySelectorAll(".btn_numero");

var bilhetesSelecionados = [];

for (var i = 0; i < links.length; i++) {
	links[i].addEventListener('click', function () {
		this.classList.toggle('btn_numero_active');
		var numeroBilhete = parseInt($(this).attr('data-bilhete'));
		console.log(parseInt(numeroBilhete))
		if (numeroBilhete || numeroBilhete == 0) {
			var bilheteIndex = bilhetesSelecionados.indexOf(numeroBilhete);
			if (bilheteIndex == -1) {
				bilhetesSelecionados.push(numeroBilhete)
			} else {
				bilhetesSelecionados.splice(bilheteIndex, 1);
			}
		}

		toggleReservarButton();

	});

}

function toggleReservarButton() {
	if (bilhetesSelecionados.length == 0) {
		$(".btn-reserva-numero").addClass('disabled')
	} else {
		$(".btn-reserva-numero").removeClass('disabled')
	}
}

var numeros = document.querySelector('.numeros');

numeros.addEventListener('click', function (e) {
	if (e.target.classList.contains('btn_numero')) {
		e.target.classList.toggle('btn_numero_active');
	}
});

var $numerosContainer = $(".numeros");

$numerosContainer.on("click", ".btn_numero", function () {
	$(this).toggleClass("btn_numero_active");
});

$(".btn-reserva-numero").click(function () {
	renderModalBadges();
	$("#modalReservaNumero").modal('show')
})

function renderModalBadges() {
	$("#numerosReservados").empty()
	bilhetesSelecionados.forEach(function (numero) {
		var el = "<button class='btn badge-button btn-reservar m-1' tabindex='-1' onclick='removeNumber(" + numero + ")'>" + numero.pad(3);
		el += " <span class='badge badge-light'>"
			+ "<span class='fa fa-trash'></span>"
			+ "</span>"
		el += "</button>";
		$("#numerosReservados").append(el)
	})
	$("#valorPagar").text((valorRifa * bilhetesSelecionados.length).toFixed(2).replace('.', ','));
}

function removeNumber(n) {
	var bilheteIndex = bilhetesSelecionados.indexOf(n);
	bilhetesSelecionados.splice(bilheteIndex, 1);
	if (bilhetesSelecionados.length == 0) {
		$("#modalReservaNumero").modal('hide')
		toggleReservarButton()
	}
	$(".btn_numero[data-bilhete='" + n + "']").removeClass('btn_numero_active')
	renderModalBadges();
}

$("#btnEnviaReserva").click(function () {
	event.preventDefault();

	if ($(formReserva['nome']).val().length < 15) {
		swal("Preencha os Campos Corretamente", "Digite o Nome Completo", "error")
	} else if ($(formReserva['telefone']).val().length < 10) {
		swal("Preencha os Campos Corretamente", "Digite um Telefone válido", "error")
	} else {
		var url = location.href.split('/'),
			id_rifa = url[url.length - 1];
		$btn = $(this);
		$.post('/api/reserva.php', {
			numeros_reserva: bilhetesSelecionados,
			nome_comprador: $(formReserva['nome']).val(),
			tel_comprador: $(formReserva['telefone']).val(),
			total_compra: valorRifa * bilhetesSelecionados.length,
			id_rifa: id_rifa
		}).done(function (res) {
			location.reload();
			$btn.html("<i class='fa fa-spinner fa-spin'>")
				.css('pointer-events', 'none')
		})
	}

})

$('#formReserva').on('keyup keypress', function (e) {
	var keyCode = e.keyCode || e.which;
	if (keyCode === 13) {
		e.preventDefault();
		return false;
	}
});

$('[data-toggle="tooltip"]').tooltip();   