$(document).ready(function () {
    // API's
    var _baseurl = "/api";

    $("#btnCadastraAnuncianteNovo").click(function (e) {
        e.preventDefault()

        if ($("#fieldNomeUser").val().length < 15) {
            err("Insira o Nome Completo");
        } else if ($("#fieldTelUser").val().length < 10) {
            err("Insira um telefone válido");
        } else if ($("#fieldMailUser").val().length < 5
            || $("#fieldMailUser").val().indexOf('@') == -1
            || $("#fieldMailUser").val().indexOf('.') == -1) {
            err("Insira um email válido");
        } else if ($("#fieldSenhaUser").val().length < 6) {
            err("Insira uma senha com mais de 6 caracteres");
        } else {
            var $btn = $(this),
                data = $("#formCadastraAnunciante").serialize();
            $btn.html("<i class='fa fa-spinner fa-spin'>")
                .css('pointer-events', 'none')

            $.post(_baseurl + '/cadastro.php', data)
                .done(function (r) {
                    $btn.text("CADASTRAR");
                    $btn.css('pointer-events', 'initial')

                    if (r == 1) {
                        swal("Sucesso", "O Usuário foi Cadastrado com Sucesso", "success")
                            .then(function () {
                                $("#modalCadastro").modal('hide')
                                $("#formCadastraAnunciante").trigger('reset')
                                location.reload();
                            })
                    }
                })
        }
    })
})

$('.btn-apaga-anunciante').click(function (e) { 
    e.preventDefault();
    var idAnuciante = $(this).attr('data-id');
    swal({
        title: 'Apagar Anunciante',
        text: 'Deseja realmente apagar o anunciante ' + $(this).attr('data-nome') + '?',
        showCancelButton: true,
        type: 'warning'
    }).then(function(r){
        if(r.value == true){
            $.post("/api/apagar_usuario.php", {
                id_anunciante: idAnuciante
            }).done(function(r){
                location.reload();
            });
        }
    })
});

$('.btn-edita-anunciante').click(function (e) { 
    e.preventDefault();
    var idAnunciante = $(this).attr('data-id');

    // allAnunciates é setada na linha 123 de perfil.php
    var anunciante = allAnunciantes.find(function(anunciante){
        return anunciante.id_anunciante = idAnunciante;
    })

    $("#btnEditaAnunciante").attr('data-id', idAnunciante);

    $("#fieldNewUserName").val(anunciante.nome);
    $("#fieldNewUserTel").val(anunciante.telefone);
    $("#fieldNewUserMail").val(anunciante.email);
    $("#fieldMaxRifas").val(anunciante.max_rifas);

    if(anunciante.tem_permissao == 1){
        $("#chbPermissaoLogin").prop('checked', true);
    } else {
        $("#chbPermissaoLogin").prop('checked', false);
    }

    $("#modalEditarAnunciante").modal("show")
});

$('#btnEditaAnunciante').click(function (e) {

    $btn = $(this);

    $btn.html("<i class='fa fa-spinner fa-spin'>")
    .css('pointer-events', 'none')

    e.preventDefault();
    if ($("#fieldNewUserName").val().length < 15) {
        err("Insira o Nome Completo");
    } else if ($("#fieldNewUserTel").val().length < 10) {
        err("Insira um telefone válido");
    } else if ($("#fieldNewUserMail").val().length < 5
        || $("#fieldNewUserMail").val().indexOf('@') == -1
        || $("#fieldNewUserMail").val().indexOf('.') == -1) {
        err("Insira um email válido");
    } else if ($("#fieldNewUserPassword").val() != "" && $("#fieldNewUserPassword").val().length < 6) {
        err("Insira uma senha com mais de 6 caracteres");
    } else if (parseInt($("#fieldMaxRifas").val()) < 0){
        err("Insira um valor máximo de rifas válido");
    } else {
        var newInfo = {
            id_anunciante: $(this).attr('data-id'),
            nome: $("#fieldNewUserName").val(),
            telefone: $("#fieldNewUserTel").val(),
            senha: $("#fieldNewUserPassword").val(),
            max_rifas: $("#fieldMaxRifas").val(),
            tem_permissao: $("#chbPermissaoLogin").prop('checked')
        }

        console.log(newInfo)
        
        $.post('/api/admin_edita_anunciante.php', newInfo)
            .done(function(r){
                location.reload();
            })
    }
});


function err(msg) {
    swal("Campo Incorreto", msg, 'error');
}