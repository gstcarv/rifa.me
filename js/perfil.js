
/*

    MODAL NOVA RIFA

*/


$('.rifa-thumbnail input').change(function () {
    var file = $(this)[0].files[0]
    var $thumb = $(this).parent();
    if (file) {
        var thumb = URL.createObjectURL(file);
        $thumb.css('background-image', "url(" + thumb + ")");
    } else {
        $thumb.css('background-image', "none");
    }
})

numFiles = 0;
compressed = [];

$("#btnCriarRifa").click(function (e) {
    e.preventDefault();

    var rifa = {
        titulo: $('#tituloRifa').val(),
        valor: $('#valorRifa').val(),
        numBilhetes: $('#numBilhetes').val(),
        descricao: $('#descricaoRifa').val()
    }

    if (rifa.titulo.length < 6) {
        err("Digite um titulo maior");
    } else if (rifa.valor <= 0) {
        err("Insira um valor válido para a Rifa");
    } else if (rifa.numBilhetes <= 0) {
        err("Insira um número válido de bilhetes");
    } else if (rifa.descricao.length <= 20) {
        err("Insira uma Descrição com no mínimo 20 caracteres");
    } else {
        var compressor = new ImageCompressor();
        $('#formNovaRifa input[type="file"]').each(function () {
            var files = $(this)[0].files[0];
            if (files) {
                numFiles++;
                compressor.compress(files, {
                    quality: .2,
                    success: function(res) {
                        compressed.push(res);
                        send(rifa);
                    }
                })
            }
        })

        if (numFiles > 0) {
            $(this).css('pointer-events', 'none')
                .html('<span class="fa fa-spinner fa-spin"></span> Enviando...')
        } else {
            err("Selecione pelo menos uma imagem");
        }

    }

    return false;
});

function send(rifa) {
    if (compressed.length > 0 && compressed.length == numFiles) {
        var data = new FormData($("#formNovaRifa"));

        var index = 1;
        compressed.forEach(function (file) {
            data.append('imagem_' + index, file)
            index++;
        })

        data.append('titulo', rifa.titulo);
        data.append('valor', rifa.valor);
        data.append('num_bilhetes', rifa.numBilhetes);
        data.append('descricao', rifa.descricao);

        $("#uploadProgressBar").text("0%");

        $.ajax({
            type: "POST",
            url: "/api/nova_rifa.php",
            data: data,
            contentType: false,
            processData: false,
            xhr: function () {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);

                        var progressString = percentComplete + '%'
                        $("#uploadProgressBar").css('width', progressString)
                            .text(progressString);
                    }
                }, false);

                return xhr;
            },
            success: function (r) {
                location.href = "/rifa/" + r;
            }
        });
        return false;
    }
}

function err(msg) {
    swal("Campo Incorreto", msg, 'error');
}

/*

    MODAL EDITAR PERFIL

*/


$("#formEditarPerfil").submit(function (e) {
    if ($("#fieldNome").val().length < 15) {
        err("Insira o Nome Completo");
        e.preventDefault();
    } else if ($("#fieldTel").val().length < 10) {
        err("Insira um telefone válido");
        e.preventDefault();
    } else if ($("#fieldMail").val().length < 5
        || $("#fieldMail").val().indexOf('@') == -1
        || $("#fieldMail").val().indexOf('.') == -1) {
        err("Insira um email válido");
        e.preventDefault();
    } else if ($("#fieldSenha").val() != "" && $("#fieldSenha").val().length < 6) {
        err("Insira uma senha com mais de 6 caracteres");
        e.preventDefault();
    }
});


/*

    MODAL NOVO BANCO

*/


$("#btnAdicionarNovoBanco").click(function(){
    event.preventDefault();
    if ($("#fieldTitularBanco").val().length < 15) {
        err("Insira o Nome Completo");
        e.preventDefault();
    } else if ($("#fieldCpfBanco").val().length < 14) {
        err("Insira um CPF válido");
        e.preventDefault();
    } else if ($("#fieldAgenciaBanco").val().length < 3) {
        err("Insira uma Agência Válida");
        e.preventDefault();
    } else if ($("#fieldContaBanco").val().length < 4) {
        err("Insira uma Conta Válida");
        e.preventDefault();
    } else {
        $.post('/api/adiciona_banco.php', $("#formNovoBanco").serialize())
        .done(function(r){
            location.reload();
        })
    }
})

/*

    APAGAR BANCO

*/


$(".btn-apagar-banco").click(function(e){
    var button = $(this);
    swal({
        title: 'Apagar Banco',
        text: 'Deseja realmente apagar esse banco?',
        showCancelButton: true,
        type: 'warning'
    }).then(function(r){
        if(r.value == true){
            var idBanco = button.attr('data-id-banco'),
                card =  $("#containerBancos div[data-id-banco='" + idBanco + "']");
            button.css('pointer-events', 'none');
            card.css('opacity', '.5')
            $.post('/api/apaga_banco_anunciante.php', {
                id_banco_anunciante: idBanco
            }).done(function(r){
                card.detach();
            })
        }
    })
})

/*

    MODAL FINALIZAR RIFA

*/


$(".btn-modal-finaliza-rifa").click(function (e) { 
    e.preventDefault();
    var total = $(this).attr('data-valor-total'),
        idRifa = $(this).attr('data-id-rifa');
        idAnunciante = $(this).attr('data-id-anunciante');

    $("#btnFinalizarRifa").attr('data-id-anunciante', idAnunciante);
    $("#btnFinalizarRifa").attr('data-id-rifa', idRifa);
    $("#btnFinalizarRifa").attr('data-valor-total', total);

    $("#totalRifa").text("R$" + parseInt(total).toFixed(2));
    $("#modalFinalizarRifa").modal("show")
});

$("#btnFinalizarRifa").click(function (e) { 
    e.preventDefault();
    if ($("#nomeGanhador").val().length < 15) {
        err("Insira o Nome Completo");
    } else if ($("#telefoneGanhador").val().length < 10) {
        err("Insira um telefone válido");
    } else if ($("#bilheteSorteado").val() == "" || parseInt($("#bilheteSorteado").val()) < 0){
        err("Insira um Bilhete Válido");
    } else {
        $(this).css('pointer-events', 'none')
        .html('<span class="fa fa-spinner fa-spin"></span> Finalizando...')
        var info = {
            nome_ganhador: $("#nomeGanhador").val(),
            tel_ganhador: $("#telefoneGanhador").val(),
            bilhete_sorteado: $("#bilheteSorteado").val(),
            id_anunciante: $(this).attr('data-id-anunciante'),
            id_rifa: $(this).attr('data-id-rifa'),
            total_pago: $(this).attr('data-valor-total'),
        }

        $.post("/api/finaliza_rifa.php", info)
            .done(function(r){
                location.reload();
            })

    }
});