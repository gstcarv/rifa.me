$(document).ready(function(){
    // PESQIOSA A RIFA
    $("#formPesquisa").submit(function(){
        event.preventDefault();
        location.href = this.action + '/' + $("#nomeComprador").val();
    })
})

// VALIDA O PAGAMENTO
$(".btn-valida-pagamento").click(function(){
    var data = $(this).attr('data-reserva');
    var comprador = $(this).attr('data-comprador');

    $.post('/api/confirma_pagamento.php', {
        nome: comprador,
        data: data
    }).done(function(r){
        location.reload();
    })
})