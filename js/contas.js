$(document).ready(function () {
    // API's
    var _baseurl = "/api";

    if (auth = localStorage.getItem('rifa.me')) {
        auth = JSON.parse(auth)
        $(formLogin['email']).val(auth.email);
        $(formLogin['senha']).val(auth.senha)
    }

    $("#btnLogin").click(function (e) {

        e.preventDefault()

        var $btn = $(this),
            data = $("#formLogin").serialize();

        $btn.html("<i class='fa fa-spinner fa-spin'>")
            .css('pointer-events', 'none')

        var lembrar = $(formLogin['chbLembrar']).is(':checked');
        if (lembrar) {
            localStorage.setItem('rifa.me', JSON.stringify({
                email: $(formLogin['email']).val(),
                senha: $(formLogin['senha']).val()
            }));
        }

        $.post(_baseurl + '/login.php', data)
            .done(function (r) {
                $btn.text("Entrar");
                $btn.css('pointer-events', 'initial')

                if (r == 1) {
                    location.href = '/perfil';
                } else {
                    swal("Erro no Login", "O Email ou a Senha estão incorretos, ou você não tem permissão de Login. Contate o Administrador", "error")
                }
            })
    })
})

function err(msg) {
    swal("Campo Incorreto", msg, 'error');
}