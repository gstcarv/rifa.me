<?php

session_start();

if(!isset($_SESSION['logged_user'])){
    header('location: /');
}

if(!isset($_GET['id_rifa'])){
    header('location: /perfil');
} else {
    $id_rifa = $_GET['id_rifa'];
}

include('api/conn.php');

$select = "SELECT *
           FROM tb_rifas
           WHERE id_rifa='$id_rifa'";

$info_rifa = $conn->query($select)->fetch_array(MYSQLI_ASSOC);

// SÓ PERMITE ENTRAR SE O USUÁRIO LOGADO FOR O DONO DA RIFA
if($info_rifa['id_anunciante'] != $_SESSION['logged_user']['id_anunciante']){
   header('location: /perfil');
}

?>
<!DOCTYPE html>
<html lang="pt-br">
   <head>
      <!-- Required meta tags -->
      <meta name="theme-color" content="#16cfb0">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
      <!-- Bootstrap CSS -->
      <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet">
      <link href="/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/css/style.css" rel="stylesheet">
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,500,800" rel="stylesheet">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
      <!-- Sweet Alert -->
      <link href="/libs/sweetalert/sweetalert.css" rel="stylesheet">
      <title>Reservas • Rife.me</title>
   </head>
   <body>
      <!-- Header -->
      <?php include('includes/PageHeader.php') ?>
      
      <!-- RESERVAS -->
      <?php
         
         $user_id = $_SESSION['logged_user']['id_anunciante'];

         $comprador = isset($_GET['comprador']) ? $_GET['comprador'] : '';
         $now = date("Y-m-d H:i:s");

         $query = "SELECT tb_bilhetes.* FROM tb_bilhetes
                   WHERE dt_reserva IS NOT NULL
                   AND nome_comprador LIKE '%$comprador%'
                   AND id_rifa='$id_rifa'
                   AND (dt_validade >= '$now' OR dt_pagamento IS NOT NULL)
                   ORDER BY dt_reserva DESC, nome_comprador ASC";

         $reservas = $conn->query($query)->fetch_all(MYSQLI_ASSOC);

      ?>
      <div>
         <div class="container">
            <div class="linha"></div>
            <div class="mt-4 mb-4">
               <a class="btn mb-3" href="javascript:history.go(-1)">Voltar</a>
               <h2>Bilhetes Reservados</h2>
               <hr>
               <form action="<?php echo '/perfil/reservas/' . $id_rifa ?>" id="formPesquisa">
                  <div class="form-row">
                     <input id="nomeComprador" value="<?php echo $comprador ?>" type="text" class="form-control" placeholder="Digite o Nome do Comprador" name="comprador">
                     <button class="btn btn-reservar btn-block mt-2">Procurar</button>
                  </div>
                  <br>
               </form>
               <?php if($comprador != ''): ?>
               <h4>Procurando por: <?php echo $comprador ?></h4>
               <br>
               <?php endif ?>
               <div>
                  <?php

                     /* 
                     
                        PASSANDO NO LAÇO DE REPERTIÇÃO:
                        SE O USUÁRIO QUE RESERVOU E A DATA E HORARIO FOREM DIFERENTES DO ULTIMO REGISTRO,
                        É CRIADO UM NOVO NOVO CARD E UMA NOVA TABELA. CASO CONTRÁRIO, AS INFORMAÇÕES
                        SÃO COLOCADAS NO MESMO CARD E NA MESMA TABELA Q O ANTERIOR
                     
                     */

                     $lastDate = null;
                     $lastName = null;
                     $quantidade = 1;
                     foreach($reservas as $index => $reserva):

                        $isDiferentDate = $reserva['dt_reserva'] != $lastDate;
                        $isDiferenteComprador = $reserva['nome_comprador'] != $lastName;
                        $lastDate = $reserva['dt_reserva'];
                        $lastName = $reserva['nome_comprador'];
                        
                        if($isDiferentDate && $isDiferenteComprador):
                           $quantidade = 1;

                  ?>
                     <div class="card">
                        <div class="card-header">
                           <?php echo date_format(date_create($reserva['dt_reserva']), 'd/m/Y H:i') ?>
                        </div>
                        <div class="card-body">
                           <span><b>Comprador</b>: <?php echo $reserva['nome_comprador'] ?></span><br>
                           <span><b>Telefone</b>: <?php echo $reserva['telefone_comprador'] ?></span>
                           <table class="table table-striped mt-3 text-center">
                              <thead>
                                 <tr>
                                    <th>Bilhete</th>
                                    <th>Valor</th>
                                 </tr>
                              </thead>
                              <tbody>
                  <?php endif ?>
                                    <tr>
                                       <td><?php echo str_pad($reserva['numero_bilhete'], 3, '0', STR_PAD_LEFT); ?></td>
                                       <td>R$<?php echo number_format($info_rifa['valor'], 2, ',', '') ?></td>
                                    </tr>
                  <?php if($index == (count($reservas) - 1)
                           || $reserva['dt_reserva'] != $reservas[$index + 1]['dt_reserva']): ?>
                              </tbody>
                           </table>
                        </div>
                        <div class="card-footer text-muted">
                           <span>Valor Total: R$<?php echo number_format($info_rifa['valor'] * $quantidade, 2, ',', '') ?></span><br>
                           <?php if(!$reserva['dt_pagamento']): ?>
                              <button class="btn mb-3 mt-3 btn-valida-pagamento" data-reserva="<?php echo $lastDate ?>" data-comprador="<?php echo $lastName ?>">Validar Pagamento</button><br>
                              <span>Validade: <?php echo date_format(date_create($reserva['dt_validade']), 'd/m/Y H:i') ?></span>
                           <?php else: ?>
                              <span>Data do Pagamento: <?php echo date_format(date_create($reserva['dt_pagamento']), 'd/m/Y H:i') ?></span>
                           <?php endif ?>
                        </div>
                     </div>
                     <br>

                  <?php endif ?>

                  <?php
                     $quantidade++; 
                     endforeach 
                  ?>

                  <?php if(count($reservas) == 0): ?>
                     <h4 style="color: gray">Nenhuma reserva encontrada</h4>
                  <?php endif ?>

               </div>
               <hr>
            </div>
         </div>
      </div>

      <!-- Footer -->
      <?php include('includes/PageFooter.php') ?>

    
      <!-- jQuery (obrigatório para plugins JavaScript do Bootstrap) -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <!-- Sweet Alert -->
      <script src="/libs/sweetalert/sweetalert.min.js"></script>
      <!-- compressor js -->
      <script src="/libs/compressor/image-compressor.min.js"></script>
      <!-- Inclui todos os plugins compilados (abaixo), ou inclua arquivos separadados se necessário -->
      <script type="text/javascript" src="/libs/bootstrap/js/bootstrap.min.js"></script>
      <script src="/js/reservas.js"></script>
   </body>
</html>